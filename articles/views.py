from django.shortcuts import render
from .models import Article
from django.http import HttpResponse

# Create your views here.


def article_list(request):
    article = Article.objects.all().order_by('date')
    return render(request, 'articles/article_list.html', {'articles': article})


def article_detail(request, slug):
    # The below line of code simply returns the url in string format
    # return HttpResponse(slug)
    article = Article.objects.get(slug=slug)
    return render(request, 'articles/detail.html', {'articles': article})
