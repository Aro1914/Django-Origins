from django.urls import path, re_path
from . import views

app_name = 'articles'

urlpatterns = [
    path('', views.article_list, name = 'home'),    
    # URL Dispatcher
    # The different ways to retrieve user requests are as follows
    # Using the re_path module for regex
    # re_path(r'^(?P<slug>[\w-]+)/$', views.article_detail),
    # Using the Request converter built into Django - This is the one for me though
    path('<str:slug>/', views.article_detail, name = 'detail'),
]
